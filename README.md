This simple python module is meant to be used as a pagemap reader under the
Linux kernel. It provides detailed data on each page mapped by a given
process. It's mainly based on the page-types tool available in the kernel's
source:

 <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/tools/vm/page-types.c>

Example:

```python
import pagemap

# By default, pages are read from the executing process. This
# example reads it from the process at PID 1.
pages_data = pagemap.ProcMaps(1)
vdso_details = pages_data.get_page_details(backing='vdso')
print(vdso_details)

# voffset cgroup map-cnt offset flags flags_s flags_l backing raw_page
# 7ffffed63 @0 208 a86ee 0x100000804 __R________M_______________r_________ referenced,mmap,reserved vdso a0800000000a86ee

for page in pages_data.get_backing_pages('vdso'):
    print(page.page_info())

# Present: True
# PFN: 0xa86ee
# Swapped: False
# file-page/shared-anon: True
# pte is uffd-wp write-protected: False
# page exclusively mapped: False
# pte is soft-dirty True

# Present: False
# Swapped: False
# file-page/shared-anon: False
# pte is uffd-wp write-protected: False
# page exclusively mapped: False
# pte is soft-dirty True

summary = pages_data.get_summary()
print(summary)

# flags page-count flags_s flags_l
# 0x0000000000000000 7 _____________________________________ none
# 0x0000000000005828 348 ___U_l_____Ma_b______________________ uptodate,lru,mmap,anonymous,swapbacked
# 0x000000000000582c 67 __RU_l_____Ma_b______________________ referenced,uptodate,lru,mmap,anonymous,swapbacked
# 0x0000000000005838 776 ___UDl_____Ma_b______________________ uptodate,dirty,lru,mmap,anonymous,swapbacked
# 0x000000000000583c 48 __RUDl_____Ma_b______________________ referenced,uptodate,dirty,lru,mmap,anonymous,swapbacked
# 0x000000000000586c 3 __RU_lA____Ma_b______________________ referenced,uptodate,lru,active,mmap,anonymous,swapbacked
# 0x0000000100000000 2137 ___________________________r_________ reserved
# 0x0000000100000800 1 ___________M_______________r_________ mmap,reserved
# 0x000000040000082c 52 __RU_l_____M_________________d_______ referenced,uptodate,lru,mmap,mappedtodisk
# 0x0000000400000868 36 ___U_lA____M_________________d_______ uptodate,lru,active,mmap,mappedtodisk
# 0x000000040000086c 1208 __RU_lA____M_________________d_______ referenced,uptodate,lru,active,mmap,mappedtodisk
# 0x000000080000006c 1 __RU_lA_______________________P______ referenced,uptodate,lru,active,private
# 0x0000002000007868 1 ___U_lA____Masb_________________O____ uptodate,lru,active,mmap,anonymous,swapcache,swapbacked,owner_private
```

`pagemap_reader.py` can be used to retrieve pages' info from the pagemap module:

```sh
usage: pagemap_reader.py [-h] [-a] [-b BACKINGS [BACKINGS ...]] [-B] [-d]
                         [-p PID] [-P PAGES [PAGES ...]] [-n]
                         [-r RANGES [RANGES ...]] [-o] [-R] [-x]

Retrieve information about memory pages

optional arguments:
  -h, --help            show this help message and exit
  -a, --aggregate       Show pages aggregated into common ranges
  -b BACKINGS [BACKINGS ...], --backing BACKINGS [BACKINGS ...]
                        Show data about pages matching given backing(s)
  -B, --show-backings   List all the available backings
  -d, --details         Show details about each page
  -p PID, --pid PID     PID of the process to lookup the pages for
  -P PAGES [PAGES ...], --page PAGES [PAGES ...]
                        Show data about a specific page
  -n, --no-summary      Do not show summary
  -r RANGES [RANGES ...], --ranges RANGES [RANGES ...]
                        Show data about pages matching given range(s)
  -o, --only-backings   Print only backings no page data
  -R, --only-ranges     Print only ranges no page data
  -x, --debug           Include zeroed pages in the details output
```
