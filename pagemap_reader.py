#!/usr/bin/env python3
"""
    Wrapper around pagemap module
"""

# pylint: disable=missing-function-docstring

import argparse
import os
import signal
import sys

try:
    import pagemap
except ImportError as error:
    print("Failed to import pagemap module: %s" % error)
    sys.exit(1)


def is_pid(pid):
    return os.path.exists("/proc/%s/status" % pid)


def get_pages_by_offset(pages):
    _pages_by_offset = {}
    for _page in pages:
        _pages_by_offset[_page.voffset] = _page
    return _pages_by_offset


def print_backings(_maps, pages=True):
    for _backing in _maps.backings:
        print(
            "%s%s"
            % (
                _backing if _backing else "anonymous",
                f": {len(maps.get_backing_pages(_backing))} pages" if pages else "",
            )
        )


def print_ranges(_maps):
    for _map in _maps.mappings:
        print("%s:" % _map if _map else "anonymous")
        for _map_ranges in _maps.mappings[_map]:
            print(
                "  0x%x-0x%x,0x%x-0x%x"
                % (
                    _map_ranges["start"],
                    _map_ranges["end"],
                    _map_ranges["pg_start"],
                    _map_ranges["pg_end"],
                )
            )


def to_int(val):
    return int(val, 0)


def sigpipe_nuller(s, f):
    sys.exit(0)


signal.signal(signal.SIGPIPE, sigpipe_nuller)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Retrieve information about memory pages"
    )
    parser.add_argument(
        "-a",
        "--aggregate",
        action="store_true",
        dest="aggregate",
        help="Show pages aggregated into common ranges",
    )
    parser.add_argument(
        "-b",
        "--backing",
        action="store",
        dest="backings",
        nargs="+",
        help="Show data about pages matching given backing(s)",
    )
    parser.add_argument(
        "-B",
        "--show-backings",
        action="store_true",
        dest="mapped_backings",
        help="List all the available backings",
    )
    parser.add_argument(
        "-d",
        "--details",
        action="store_true",
        dest="details",
        help="Show details about each page",
    )
    parser.add_argument(
        "-p",
        "--pid",
        action="store",
        dest="pid",
        default="self",
        help="PID of the process to lookup the pages for",
    )
    parser.add_argument(
        "-P",
        "--page",
        action="store",
        dest="pages",
        nargs="+",
        type=to_int,
        help="Show data about a specific page",
    )
    parser.add_argument(
        "-n",
        "--no-summary",
        action="store_false",
        dest="summary",
        help="Do not show summary",
    )
    parser.add_argument(
        "-r",
        "--ranges",
        action="store",
        dest="ranges",
        nargs="+",
        type=to_int,
        help="Show data about pages matching given range(s)",
    )
    parser.add_argument(
        "-o",
        "--only-backings",
        action="store_true",
        dest="only_backings",
        help="Print only backings no page data",
    )
    parser.add_argument(
        "-R",
        "--only-ranges",
        action="store_true",
        dest="only_ranges",
        help="Print only ranges no page data",
    )
    parser.add_argument(
        "-x",
        "--debug",
        action="store_true",
        dest="debug",
        help="Include zeroed pages in the details output",
    )

    args = parser.parse_args()

    if not is_pid(args.pid):
        print('PID "%s" does not exist' % args.pid)
        sys.exit(1)

    # pylint: disable=invalid-name
    map_backings = None
    map_ranges = None
    no_pages = False

    if args.backings:
        map_backings = args.backings
    if args.ranges:
        map_ranges = args.ranges
    if args.only_backings or args.only_ranges:
        no_pages = True
    maps = pagemap.ProcMaps(
        args.pid, backing=map_backings, start=map_ranges, no_pages=no_pages
    )

    if args.aggregate:
        print(maps.get_aggregated_ranges())
    if args.mapped_backings and not args.only_backings:
        print_backings(maps)
    if args.only_backings:
        print_backings(maps, False)
    if args.only_ranges:
        print_ranges(maps)
    if args.details:
        print(maps.get_page_details(debug=args.debug))
    elif args.backings:
        for backing in args.backings:
            if backing not in maps.backings:
                print("Skipping invalid backing %s" % backing, file=sys.stderr)
                continue
            if args.pages:
                pages_by_offset = get_pages_by_offset(maps.get_backing_pages(backing))
                for page in args.pages:
                    if page in pages_by_offset:
                        print(
                            maps.get_page_details(
                                backing=backing, voffset=page, debug=args.debug
                            )
                        )
            else:
                print(maps.get_page_details(backing=backing, debug=args.debug))
    elif args.pages:
        for page in args.pages:
            print(maps.get_page_details(voffset=page, debug=args.debug))
    if args.summary:
        print(maps.get_summary())
