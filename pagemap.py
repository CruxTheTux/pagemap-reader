"""
    pagemap reader
"""

import os
import re
import struct

from ctypes import c_uint64, sizeof
from resource import getpagesize


__all__ = [
    "MAX_VMAS",
    "PAGESIZE",
    "UINT64",
    "KPAGEFLAGS",
    "KPAGEFLAGS_FD",
    "KPAGECOUNT",
    "KPAGECOUNT_FD",
    "KPAGECGROUP",
    "KPAGECGROUP_FD",
    "ProcMaps",
    "Page",
]

MAX_VMAS = 10240
PAGESIZE = getpagesize()
UINT64 = sizeof(c_uint64)

# Use these as globals to handle subsequent open() calls
KPAGEFLAGS = "/proc/kpageflags"
KPAGECOUNT = "/proc/kpagecount"
KPAGECGROUP = "/proc/kpagecgroup"
KPAGEFLAGS_FD = None
KPAGECOUNT_FD = None
KPAGECGROUP_FD = None

if os.getuid() != 0:
    raise ImportError("You need to be root to use %s" % __file__)


def global_open():
    """
    Opens KPAGEFLAGS, KPAGECOUNT, KPAGECGROUP in global scope.
    """
    for file in [KPAGEFLAGS, KPAGECOUNT, KPAGECGROUP]:
        if not os.path.exists(file):
            raise FileNotFoundError("Cannot open %s" % file)

    # Open KPAGEFLAGS, KPAGECOUNT, KPAGECGROUP only once to avoid running
    # through open()s for each page.
    # pylint: disable=global-statement
    global KPAGEFLAGS_FD
    global KPAGECOUNT_FD
    global KPAGECGROUP_FD

    KPAGEFLAGS_FD = open(KPAGEFLAGS, "rb")
    KPAGECOUNT_FD = open(KPAGECOUNT, "rb")
    KPAGECGROUP_FD = open(KPAGECGROUP, "rb")


def global_close():
    """
    Closes KPAGEFLAGS, KPAGECOUNT, KPAGECGROUP.
    """
    if not KPAGEFLAGS_FD.closed:
        KPAGEFLAGS_FD.close()
    if not KPAGECOUNT_FD.closed:
        KPAGECOUNT_FD.close()
    if not KPAGECGROUP_FD.closed:
        KPAGECGROUP_FD.close()


def uint64_read(fileno, offset, count=1):
    """
    Performs UINT64 read from fileno at given offset.
    """
    data = os.pread(fileno, UINT64 * count, offset)
    if len(data) == UINT64 * count:
        return data
    return None


def get_iter(arg):
    """
    Return iterable for given arg
    """
    if arg is None:
        return None
    if isinstance(arg, (list, tuple)):
        return arg
    return [arg]


class ProcMaps:
    """
    Main class providing interfaces for reading /proc/%u/pagemap,
    /proc/%u/maps files.
    """

    def __init__(self, pid="self", start=None, backing=None, no_pages=False):
        self.pid = pid
        self.nr_vmas = 0
        self.start = get_iter(start)
        self.backing = get_iter(backing)
        self.mappings = self.map_maps(no_pages)
        self.backings = self.get_backings()

    def get_backings(self):
        """
        Returns list of all files backing up given ranges.
        """
        backings = [backing for backing in self.mappings if backing is not None]
        return backings

    def _get_backing_pages(self, backing):
        if self.mappings.get(backing) is not None:
            for backing_range in self.mappings[backing]:
                for page in backing_range["pages"]:
                    yield page

    def get_backing_pages(self, backing):
        """
        Returns list of Page() objects for each page mapped to
        a given backing file.
        """
        return list(self._get_backing_pages(backing))

    def get_all_backing_pages(self):
        """
        Returns list of Page() objects for all mapped pages.
        """
        all_pages = []
        for backing in self.backings:
            for page in self.get_backing_pages(backing):
                all_pages.append(page)
        return all_pages

    def get_all_backing_pages_voffset(self):
        """
        Returns list of Page() objects for all mapped pages indexed
        by their voffset.
        """
        all_pages_voffset = {}
        for page in self.get_all_backing_pages():
            all_pages_voffset[page.voffset] = page
        return all_pages_voffset

    def get_backing_page_voffset(self, voffset):
        """
        Returns Page() object matching page at a given voffset.
        """
        all_pages_voffset = self.get_all_backing_pages_voffset()
        if voffset in all_pages_voffset:
            return all_pages_voffset[voffset]
        return None

    def map_maps(self, no_pages=False):
        """
        Returns dict of start, end addresses of each mapping for
        given PID.
        """
        if not os.path.exists("/proc/%s/maps" % self.pid):
            raise FileNotFoundError("Cannot open maps file for %s" % self.pid)
        mappings = {}

        with open("/proc/%s/maps" % self.pid, "r") as maps:
            for region in maps.readlines():
                reg = region.strip().split()
                backing = reg[5] if len(reg) >= 6 else ""
                vbacking = re.match(r"\[([\w]+)\]", backing)
                if vbacking is not None:
                    (backing,) = vbacking.groups()
                elif backing:
                    backing = os.path.basename(backing)
                start, end = re.search(r"([a-z0-9]+)-([a-z0-9]+)", reg[0]).groups()
                start = int(start, base=16)
                end = int(end, base=16)
                if self.start is not None and start not in self.start:
                    continue
                if self.backing is not None and backing not in self.backing:
                    continue
                if mappings.get(backing) is None:
                    mappings[backing] = []
                mappings[backing].append(
                    {
                        "start": start,
                        "end": end,
                        "pg_start": start // PAGESIZE,
                        "pg_end": end // PAGESIZE,
                        "pages": [] if no_pages else self.get_map_range(start, end),
                    }
                )
                self.nr_vmas += 1
                if self.nr_vmas >= MAX_VMAS:
                    break
        return mappings

    def get_map_range(self, start, end):
        """
        Returns list of pages for given mapped range for
        the selected backing file of given PID.
        """
        pages = []
        offsets = []

        for offset in range(start, end, PAGESIZE):
            offsets.append(offset * UINT64 // PAGESIZE)
        pages_count = (end - start) // PAGESIZE

        if not os.path.exists("/proc/%s/pagemap" % self.pid):
            raise FileNotFoundError("Cannot open pagemap file for %s" % self.pid)

        with open("/proc/%s/pagemap" % self.pid, "rb") as pagemap:
            data = uint64_read(pagemap.fileno(), offsets[0], pages_count)

        if data:
            for idx, addr in enumerate(struct.unpack("%uQ" % pages_count, data)):
                pages.append(Page(addr, offsets[idx]))
        return pages

    def get_page_details(self, **kwargs):
        """
        Returns string describing, in detail, each present|swapped page
        for given backing or a specific page:
        voffset cgroup map-cnt offset flags flags_s flags_l
        """
        backing = None
        page = None
        iter_backings = None
        voffset = None
        debug = False
        details = ""

        if kwargs.get("page") and isinstance(kwargs["page"], Page):
            page = kwargs["page"]
        if kwargs.get("backing") and kwargs["backing"] in self.mappings:
            backing = kwargs["backing"]
            iter_backings = [backing]
        else:
            iter_backings = self.backings
        if kwargs.get("voffset"):
            voffset = kwargs["voffset"]
        if kwargs.get("debug") and kwargs["debug"] is True:
            debug = True

        def _get_page_details(page, backing=None):
            details = ""
            offset = 0

            if not isinstance(page, Page):
                return details

            if page.present or page.swapped or debug:
                if page.present:
                    offset = page.pfn
                if page.swapped:
                    offset = page.swap_offset
                details += "%016x\t%5s\t%5u\t%x\t0x%016x\t%36s\t%s " % (
                    page.voffset,
                    f"@{page.cgroup}",
                    page.count,
                    offset,
                    page.flags,
                    page.parse_page_flags("short"),
                    page.parse_page_flags("long"),
                )
                details += "N/A " if not backing else backing + " "
                details += "%x\n" % (page.page,)
            return details

        if page is not None and backing is None:
            return _get_page_details(page)

        if page is not None and backing is not None:
            if page in self.get_backing_pages(backing):
                return _get_page_details(page, backing)
            return details

        if voffset is not None:
            _page = self.get_backing_page_voffset(voffset)
            if _page is not None:
                return _get_page_details(_page, backing)
            return details

        for _backing in iter_backings:
            for _page in self.get_backing_pages(_backing):
                details += _get_page_details(_page, _backing)
        return details

    def get_flags_count(self):
        """
        Returns dict of flags' count.
        """
        flags_summary = {}

        for backing in self.backings:
            for page in self.get_backing_pages(backing):
                if not flags_summary.get(page.flags):
                    flags_summary[page.flags] = {}
                    flags_summary[page.flags]["count"] = 1
                    flags_summary[page.flags]["short"] = page.parse_page_flags()
                    flags_summary[page.flags]["long"] = page.parse_page_flags("long")
                else:
                    flags_summary[page.flags]["count"] += 1
        return flags_summary

    def get_summary(self):
        """
        Returns string representing summary of detected flags.
        flags page-count flags_s flags_l
        """
        summary = ""
        flags = self.get_flags_count()
        flags_sorted = list(flags.keys())
        flags_sorted.sort()
        for flag in flags_sorted:
            summary += "0x%016x\t%6u\t%36s\t%s\n" % (
                flag,
                flags[flag]["count"],
                flags[flag]["short"],
                flags[flag]["long"],
            )
        return summary

    def aggregate_ranges(self):
        """
        Returns dict of pages grouped into a range with matching
        attributes.
        """
        a_r = {}
        prev_page = None
        for backing in self.backings:
            for page in self.get_backing_pages(backing):
                if not page.present and not page.swapped:
                    prev_page = None
                    continue
                if page.present:
                    offset = page.pfn
                else:
                    offset = page.swap_offset
                if prev_page is not None:
                    if prev_page.present:
                        prev_offset = prev_page.pfn
                    else:
                        prev_offset = prev_page.swap_offset
                    if (
                        page.flags == prev_page.flags
                        and page.cgroup == prev_page.cgroup
                        and page.count == prev_page.count
                        and offset == prev_offset + a_r[prev_page.voffset]["count"]
                    ):
                        a_r[prev_page.voffset]["count"] += 1
                        continue
                prev_page = page
                a_r[page.voffset] = {"count": 1, "page": page}
        return a_r

    def get_aggregated_ranges(self):
        """
        Returns string describing pages grouped into a common range.
        voffset cgroup map-cnt len flags_s
        """
        ranges = self.aggregate_ranges()
        sorted_ranges = list(ranges.keys())
        sorted_ranges.sort()
        details = ""

        for _range in sorted_ranges:
            details += "%x\t5%s\t%u\t%u\t%36s\n" % (
                _range,
                f"@{ranges[_range]['page'].cgroup}",
                ranges[_range]["page"].count,
                ranges[_range]["count"],
                ranges[_range]["page"].parse_page_flags("short"),
            )
        return details


class Page:
    """
    Helper class which exposes interfaces for reading data of given
    pages mapped to a given process.
    """

    # pylint: disable=too-many-instance-attributes
    def __init__(self, page=0, offset=0):
        pfn_mask = (1 << 55) - 1
        swap_offset_mask = (1 << 50) - 1
        swap_type_offset = (1 << 5) - 1

        self.page = page
        self.swap_offset = (self.page >> 5) & swap_offset_mask
        self.present = bool(self.page & 1 << 63)
        self.swapped = bool(self.page & 1 << 62)
        self.fp_sa = bool(self.page & 1 << 61)
        self.pte_is_wp = bool(self.page & 1 << 57)
        self.excl_mapped = bool(self.page & 1 << 56)
        self.pte_is_sd = bool(self.page & 1 << 55)
        self.swap_type = self.page & swap_type_offset
        self.pfn = self.page & pfn_mask
        self.flags = self.get_page_flags()
        self.offset = offset
        self.voffset = offset // UINT64
        self.count = self.get_page_count()
        self.cgroup = self.get_page_cgroup()

    def get_page_flags(self):
        """
        Returns int representation of given set of flags.
        """
        data = uint64_read(KPAGEFLAGS_FD.fileno(), self.pfn * UINT64)
        if data:
            return struct.unpack("Q", data)[0]
        return 0

    def get_page_count(self):
        """
        Returns map count of given page.
        """
        data = uint64_read(KPAGECOUNT_FD.fileno(), self.pfn * UINT64)
        if data:
            return struct.unpack("Q", data)[0]
        return 0

    def get_page_cgroup(self):
        """
        Returns cgroup inode of given page.
        """
        data = uint64_read(KPAGECGROUP_FD.fileno(), self.pfn * UINT64)
        if data:
            return struct.unpack("Q", data)[0]
        return 0

    def parse_page_flags(self, flag_format="short"):
        """
        Returns string representation of given set of flags.
        See: include/uapi/linux/kernel-page-flags.h and
        include/linux/kernel-page-flags.h.
        """
        flags = [
            {"short": "L", "long": "locked", "bit": 0},
            {"short": "E", "long": "error", "bit": 1},
            {"short": "R", "long": "referenced", "bit": 2},
            {"short": "U", "long": "uptodate", "bit": 3},
            {"short": "D", "long": "dirty", "bit": 4},
            {"short": "l", "long": "lru", "bit": 5},
            {"short": "A", "long": "active", "bit": 6},
            {"short": "S", "long": "slab", "bit": 7},
            {"short": "W", "long": "writeback", "bit": 8},
            {"short": "I", "long": "reclaim", "bit": 9},
            {"short": "B", "long": "buddy", "bit": 10},
            {"short": "M", "long": "mmap", "bit": 11},
            {"short": "a", "long": "anonymous", "bit": 12},
            {"short": "s", "long": "swapcache", "bit": 13},
            {"short": "b", "long": "swapbacked", "bit": 14},
            {"short": "H", "long": "compound_head", "bit": 15},
            {"short": "T", "long": "compound_tail", "bit": 16},
            {"short": "G", "long": "huge", "bit": 17},
            {"short": "u", "long": "unevictable", "bit": 18},
            {"short": "X", "long": "hwpoison", "bit": 19},
            {"short": "n", "long": "nopage", "bit": 20},
            {"short": "x", "long": "ksm", "bit": 21},
            {"short": "t", "long": "thp", "bit": 22},
            {"short": "o", "long": "offline", "bit": 23},
            {"short": "g", "long": "pgtable", "bit": 24},
            {"short": "z", "long": "zero_page", "bit": 25},
            {"short": "i", "long": "idle_page", "bit": 26},
            {"short": "r", "long": "reserved", "bit": 32},
            {"short": "m", "long": "mlocked", "bit": 33},
            {"short": "d", "long": "mappedtodisk", "bit": 34},
            {"short": "P", "long": "private", "bit": 35},
            {"short": "p", "long": "private_2", "bit": 36},
            {"short": "O", "long": "owner_private", "bit": 37},
            {"short": "h", "long": "arch", "bit": 38},
            {"short": "c", "long": "uncached", "bit": 39},
            {"short": "f", "long": "softdirty", "bit": 40},
            {"short": "H", "long": "arch_2", "bit": 41},
        ]

        if flag_format == "short":
            flags_parsed = ""
        else:
            flags_parsed = []

        for flag in flags:
            if self.flags & 1 << flag["bit"]:
                if flag_format == "short":
                    flags_parsed += flag["short"]
                else:
                    flags_parsed.append(flag["long"])
            elif flag_format == "short":
                flags_parsed += "_"
        if flag_format == "short":
            return flags_parsed
        return ",".join(flags_parsed) if flags_parsed else "none"

    def page_info(self):
        """
        Print page data:
        * Bits 0-54  page frame number (PFN) if present
        * Bits 0-4   swap type if swapped
        * Bits 5-54  swap offset if swapped
        * Bit  55    pte is soft-dirty
        * Bit  56    page exclusively mapped (since 4.2)
        * Bit  57    pte is uffd-wp write-protected (since 5.13)
        * Bits 58-60 zero
        * Bit  61    page is file-page or shared-anon (since 3.5)
        * Bit  62    page swapped
        * Bit  63    page present
        """

        info = ""
        info += "Present: %s\n" % (self.present,)
        if self.present:
            info += "PFN: 0x%x\n" % (self.pfn,)
        info += "Swapped: %s\n" % (self.swapped,)
        if self.swapped:
            info += "swap offset: 0x%x\n" % (self.swap_offset,)
            info += "swap type: 0x%x\n" % (self.swap_type,)
        info += "file-page/shared-anon: %s\n" % (self.fp_sa,)
        info += "pte is uffd-wp write-protected: %s\n" % (self.pte_is_wp,)
        info += "page exclusively mapped: %s\n" % (self.excl_mapped,)
        info += "pte is soft-dirty %s\n" % (self.pte_is_sd,)
        return info


global_open()
